import React, { Component } from 'react';
import Users from './Users';
import AddUser from './AddUser';

class App extends Component{

    state = {

        users:[
          {name:"Иванов Иван иванович", age:18, isEditing:false},
          {name:"Петров Игорь Алексеевич", age:22, isEditing:false},
          {name:"Солопов Алексей Вячеславович", age:21, isEditing:false}
    
        ]
      }

      addUser = (newUser) => {
            let users = [...this.state.users, newUser];
            this.setState({
                users
            });     
      }

      pressEditBtn = (i) => {
        let users = this.state.users;
        users[i].isEditing = true;
        this.setState({
            users
        });
      }

      updateUser = (i, name, age) => {
        let users = this.state.users;
        users[i].name = name;
        users[i].age = age;
        users[i].isEditing = false;

        this.setState({
            users
        });

      }

      pressDelete = (i) => {
        let users = this.state.users.filter((u,index)=>{
            return index !== i;
        });
        this.setState({
            users
        });
      }

    render(){
        return(
            <div className="container">
                <h1>CRUD-UI</h1>
                <Users allUsers={this.state.users} pressEditBtn={this.pressEditBtn} updateUser={this.updateUser} pressDelete={this.pressDelete} />
                <AddUser addUser={this.addUser}/>
            </div>
        );
    }
}

export default App;